<?php

/**
 * @file common.inc.php common function definitions for xmlrpc services
 *   XML RPC Server for GreenNet Services
 *
 * @author
 *   Michael Moritz <mimo at gn.apc.org>
 *   10/2006
 *
 * @TODO rewrite so that any calls to sudo or executing
 *       allow for arrays of arguments only so that they
 *       will always be escapeshellarg'ed
**/
define("VERSION","2");
define("AUTHOR","mimo");
define("RELEASE","2008/04/03");

openlog('xmlprc',LOG_CONS|LOG_PID,LOG_SYSLOG);
syslog(LOG_INFO,$_SERVER['PHP_SELF']. " started:"
  ." method=".$_SERVER['REQUEST_METHOD']
  ." user agent=".$_SERVER['HTTP_USER_AGENT']
  ." remote addr=".$_SERVER['REMOTE_ADDR']
  ." remote port=".$_SERVER['REMOTE_PORT']
  ." auth digest=".$_SERVER['PHP_AUTH_DIGEST']
  ." auth user=".$_SERVER['PHP_AUTH_USER']
  ." auth type=".$_SERVER['AUTH_TYPE']);
function __get_error($msg=false,$set=false) {
	static $_err;
	if(!is_array($_err)) {
		$_err = array();
	}
	if($set) {
		$_err[] = $msg;
	}
	return $_err;
}
/**
 *  report an error to the global error object
**/
function _error($msg) {
	__get_error($msg,true);
	syslog(LOG_INFO,(is_array($msg)?implode(", ",$msg):$msg));
}
/**
 *  check for error condition -- return true if
 *		something went wrong
**/
function _is_error() {
	if(count(__get_error())) {
		return true;
	}
	return false;
}
function _get_error_array() {
	return __get_error();
}
function _get_error_fmt() {
	return print_r(_get_error_array(), TRUE);

}
/**
 * universal _check_args
**/
function _check_args($args,$required) {
	if(!is_array($args)) {
		_error( __FUNCTION__.": should be an array: $args -- required: "
			. print_r($required,TRUE) );
		return false;
	}
	foreach($required as $key) {
		if( ! array_key_exists($key,$args) ) {
		_error( __FUNCTION__.": missing required arg: ".$key);
		return false;
		}
	}
	return true;
}
function _is_debug($function,$set=false,$set_to=false) {
	static $func_debug = array();
	$function = strtolower($function);
	if($set) {
		$func_debug[$function] = $set_to;
		return $set_to;
	}
	if(array_key_exists($function,$func_debug))
		return $func_debug[$function];
	switch($function) {
// 		case 'mailman.find_member':
// 			return false;
	}
	return false;
}
function _get_stream_contents($handle) {
	$contents = '';
 	while (!feof($handle)) {
   		$contents .= fread($handle, 8192);
 	}
 	fclose($handle);
	return $contents;
}

function _shell_command($cmd,$callback=0)
{
	$ds = array(
		0 => array('pipe','r'),
		1 => array('pipe','w'),
		2 => array('pipe','w') );
	$process = proc_open($cmd, $ds, $pipes);
	if(!is_resource($process))
		return array("error"=>"something went wrong when executing $cmd");
	if($callback['func']) {
// 		$args_array = array('pipes'=>&$ds,'args'=>$callback['args']);
		$callback['args']['pipes'] = &$pipes;
 		call_user_func_array($callback['func'],array(&$callback['args']));
//		_test_shell_command_callback($callback['args']);
		unset($callback['args']['pipes']);
	}
	$result_stdout = _get_stream_contents($pipes[1]);
	$result_stderr = _get_stream_contents($pipes[2]);
	$return = proc_close($process);
	return array(
		'stdout' => $result_stdout,
		'stderr' => $result_stderr,
		'result' => $return );
}
function _do_sudo($cmd,$caller)
{
	if(_is_debug($caller))
		return array('result'=>0,'stderr'=>'',
			'stdout'=>"$caller: would execute: $cmd");
	else
		$res = _shell_command('sudo '.$cmd);
	return $res;
}

/**
  * call a function - blocked if an error occured
**/
function _call_mysql_func() {
	if(_is_error()) {
		return;
	}
	$args = func_get_args();
	$func = array_shift($args);
	if(!function_exists($func)) {
		_error(__FUNCTION__." tried calling $func");
		return;
	}
	$result = call_user_func_array($func,$args);
	if(mysql_error() != '') {
		_error(__FUNCTION__." func: $func ".mysql_error());
	}
	return $result;
}

/// Construct the XML RPC Server
require_once("xmlrpc.inc.php");

$server = new IXR_IntrospectionServer();
// $server = new IXR_Server();

function system_set_debug($function) {//,$set_to) {
	global $server;
	if(!$server->hasMethod($function) && !function_exists($function))
		return new IXR_Error(9999,"No such function or method:   ".var_export($function,TRUE));
	_is_debug($function,true,true);
	return true;
}
$server->addCallback(
    'system.set_debug',  // XML-RPC method name
    'system_set_debug',       // function to calback
    array('boolean','string'), // Array specifying the method signature
    'enables debug for this function'  // Documentation string
);

function system_get_date($args) {
    return date($args).(_is_debug(__FUNCTION__)?' (debug mode)':'');
}

// Now add the callbacks along with their introspection details
$server->addCallback(
    'system.get_date',  // XML-RPC method name
    'system_get_date',       // function to calback
    array('string','string'), // Array specifying the method signature
    'Returns the current date as a string'  // Documentation string
);
function system_get_server_info($args) {
    return array('stdout' => 
	'server_time: '. date($args).(_is_debug(__FUNCTION__)?' (debug mode)':'')."\n".
	'server_version: ' .VERSION."\n".
	'server_author: '. AUTHOR."\n".
	'server_release:'. RELEASE."\n",
      'stderr' => '',
      'result' => 0
	
    );
}
$server->addCallback(
    'system.get_server_info',  // XML-RPC method name
    'system_get_server_info',       // function to calback
    array('string','array'), // Array specifying the method signature
    'Returns server info'  // Documentation string
);
function system_test_error($args) {
	return array(
		'stdout' => "stdout: $args",
		'stderr' => "stderr: $args",
		'result' => "-1" );
}
$server->addCallback(
    'system.test_error',  // XML-RPC method name
    'system_test_error',       // function to calback
    array('string','array'), // Array specifying the method signature
    'Returns a typical error from a sudo script'  // Documentation string
);
?>
